import sys
import os
from wsgiref import simple_server, util
from pytz import timezone
from datetime import datetime
from urllib.parse import parse_qs
import json

# web app function
def app(environ, respond):

    # req = os.path.join(path, environ['PATH_INFO'][1:])
    
    res_str = ''
    dt_format = '%m.%d.%Y %H:%M:%S'
    method = environ['REQUEST_METHOD']

    if method == 'GET':
        # read timezone param
        time_zone = parse_qs(environ['QUERY_STRING']).get('tz', [''])[0] 

        if time_zone == '':
            time_zone = 'Etc/GMT'

        # get date and time
        dt_str = (datetime.now().astimezone(timezone(time_zone))).strftime(dt_format)
        res_str = 'Current date and time for timezone ' + str(time_zone) + ': ' + dt_str

    else: # method == 'POST'
        # read timezone param

        # read request data
        req_len = int(environ.get('CONTENT_LENGTH', 0))
        req_data = environ['wsgi.input'].read(req_len).decode()
        data_params_dict = json.loads(req_data)

        # convert dict to subscriptable list
        data_params_list = []
        for key,value in data_params_dict.items() :
            data_params_list.append(value)

        n_params = len(data_params_list)

        if n_params == 2: # time convert request

            target_tz = parse_qs(environ['QUERY_STRING']).get('target_tz', [''])[0] 

            # get current timezone date and time
            prev_tz = data_params_list[1]
            prev_dt = timezone(prev_tz).localize(datetime.strptime(data_params_list[0], dt_format))

            # convert date and time to target timezone date and time
            target_dt = prev_dt.astimezone(timezone(target_tz))
            res_str = 'Current date and time for ' + str(prev_tz) + ': ' + prev_dt.strftime(dt_format) + ', and for ' + str(target_tz) + ': ' + target_dt.strftime(dt_format)
        
        else: # time difference request (n_params == 4)
            # get timezone date and time
            first_tz = data_params_list[1]
            first_dt = timezone(first_tz).localize(datetime.strptime(data_params_list[0], dt_format))

            dt_format2 = '%H:%M%p %Y-%m-%d'

            second_tz = data_params_list[3]
            second_dt = timezone(second_tz).localize(datetime.strptime(data_params_list[2], dt_format2))

            # convert timezone date and time to GMT
            first_dt_gmt = first_dt.astimezone(timezone('GMT'))
            second_dt_gmt = second_dt.astimezone(timezone('GMT'))

            # difference between date and time in seconds
            diff = (first_dt_gmt - second_dt_gmt).total_seconds()
            res_str = 'Difference between ' + first_dt.strftime(dt_format) + ' ' + str(first_tz) + ' and ' + second_dt.strftime(dt_format2) + ' ' + str(second_tz) + ' in seconds: ' + str(diff)

    res_html = '''
      <html>
      <body>
        <h3>''' + res_str + '''</h3>
      </body>
      </html>
    '''

    respond('200 OK', [('Content-Type', 'text/html'),('Content-Length', str(len(res_html)))])
    return [res_html.encode()]

if __name__ == '__main__':
    path = sys.argv[1] if len(sys.argv) > 1 else os.getcwd()
    port = int(sys.argv[2]) if len(sys.argv) > 2 else 8000
    httpd = simple_server.make_server('', port, app)
    print("Serving {} on port {}, control-C to stop".format(path, port))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        print("Shutting down.")
        httpd.server_close()