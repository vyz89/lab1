import requests
r1 = requests.get("http://127.0.0.1:8000?tz=Europe/Moscow")
print(r1.text)
r2 = requests.get("http://127.0.0.1:8000?tz=")
print(r2.text)
r3 = requests.post("http://127.0.0.1:8000?target_tz=Asia/Novosibirsk", data = '{"date":"12.20.2021 22:21:05", "tz":"EST"}')
print(r3.text)
r4 = requests.post("http://127.0.0.1:8000", data = '{"first_date":"12.20.2021 22:21:05", "first_tz":"EST","second_date":"12:20pm 2020-12-01","second_tz":"Europe/Moscow"}')
print(r4.text)